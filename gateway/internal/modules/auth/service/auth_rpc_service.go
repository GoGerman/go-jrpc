package service

import (
	"context"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/client/rpc"
	error2 "gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/error"
)

type AuthServiceJSONRPC struct {
	client rpc.Client
}

func NewAuthServiceJSONRPC(client rpc.Client) *AuthServiceJSONRPC {
	u := &AuthServiceJSONRPC{client: client}

	return u
}

func (t *AuthServiceJSONRPC) Register(ctx context.Context, in RegisterIn) RegisterOut {
	var out RegisterOut
	err := t.client.Call("AuthServiceJSONRPC.Register", in, &out)
	if err != nil {
		out.ErrorCode = error2.AuthServiceGeneralErr
	}

	return out
}

func (t *AuthServiceJSONRPC) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	var out AuthorizeOut
	err := t.client.Call("AuthServiceJSONRPC.AuthorizeEmail", in, &out)
	if err != nil {
		out.ErrorCode = error2.AuthServiceGeneralErr
	}

	return out
}

func (t *AuthServiceJSONRPC) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	var out AuthorizeOut
	err := t.client.Call("AuthServiceJSONRPC.AuthorizeRefresh", in, &out)
	if err != nil {
		out.ErrorCode = error2.AuthServiceGeneralErr
	}

	return out
}

func (t *AuthServiceJSONRPC) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	var out AuthorizeOut
	err := t.client.Call("AuthServiceJSONRPC.AuthorizeOut", in, &out)
	if err != nil {
		out.ErrorCode = error2.AuthServiceGeneralErr
	}

	return out
}

func (t *AuthServiceJSONRPC) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	var out SendPhoneCodeOut
	err := t.client.Call("AuthServiceJSONRPC.SendPhoneCode", in, &out)
	if err != nil {
		//
	}

	return out
}

func (t *AuthServiceJSONRPC) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	var out VerifyEmailOut
	err := t.client.Call("AuthServiceJSONRPC.VerifyEmail", in, &out)
	if err != nil {
		out.ErrorCode = error2.AuthServiceGeneralErr
	}

	return out
}
