package service

import (
	"context"
	"gitlab.com/GoGerman/go-jrpc/gateway/gRPC/proto"
	errors "gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/error"
)

type AuthServiceGRPC struct {
	client proto.AuthServiceRPCClient
}

func NewAuthServiceGRPC(client proto.AuthServiceRPCClient) *AuthServiceGRPC {
	a := &AuthServiceGRPC{client: client}
	return a
}

func (a *AuthServiceGRPC) Register(ctx context.Context, in RegisterIn) RegisterOut {
	req, err := a.client.Register(ctx, &proto.RegisterIn{
		Email:    in.Email,
		Password: in.Password,
		//IdempotencyKey: string(rune(field)),
	})
	if err != nil {
		return RegisterOut{
			ErrorCode: errors.AuthServiceGeneralErr,
		}
	}
	return RegisterOut{
		Status:    int(req.GetStatus()),
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (a *AuthServiceGRPC) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	req, err := a.client.AuthorizeEmail(ctx, &proto.AuthorizeEmailIn{
		Email:          in.Email,
		Password:       in.Password,
		RetypePassword: in.RetypePassword,
	})
	if err != nil {
		return AuthorizeOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	return AuthorizeOut{
		UserID:       int(req.GetUserId()),
		AccessToken:  req.GetAccessToken(),
		RefreshToken: req.GetRefreshToken(),
		ErrorCode:    int(req.GetErrorCode()),
	}
}

func (a *AuthServiceGRPC) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	req, err := a.client.AuthorizeRefresh(ctx, &proto.AuthorizeRefreshIn{
		UserId: int32(in.UserID),
	})
	if err != nil {
		return AuthorizeOut{
			ErrorCode: errors.AuthServiceRefreshTokenGenerationErr,
		}
	}
	return AuthorizeOut{
		UserID:       int(req.GetUserId()),
		AccessToken:  req.GetAccessToken(),
		RefreshToken: req.GetRefreshToken(),
		ErrorCode:    int(req.GetErrorCode()),
	}
}

func (a *AuthServiceGRPC) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	req, err := a.client.AuthorizePhone(ctx, &proto.AuthorizePhoneIn{
		Phone: in.Phone,
		Code:  int32(in.Code),
	})
	if err != nil {
		return AuthorizeOut{
			ErrorCode: errors.UserServiceWrongPhoneCodeErr,
		}
	}
	return AuthorizeOut{
		UserID:       int(req.GetUserId()),
		AccessToken:  req.GetAccessToken(),
		RefreshToken: req.GetRefreshToken(),
		ErrorCode:    int(req.GetErrorCode()),
	}
}

func (a *AuthServiceGRPC) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	req, err := a.client.SendPhoneCode(ctx, &proto.SendPhoneCodeIn{
		Phone: in.Phone,
	})
	if err != nil {
		return SendPhoneCodeOut{
			Code: errors.UserServiceWrongPhoneCodeErr,
		}
	}
	return SendPhoneCodeOut{
		Phone: req.GetPhone(),
		Code:  int(req.GetCode()),
	}
}

func (a *AuthServiceGRPC) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	req, err := a.client.VerifyEmail(ctx, &proto.VerifyEmailIn{
		Hash:  in.Hash,
		Email: in.Email,
	})
	if err != nil {
		return VerifyEmailOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	return VerifyEmailOut{
		Success:   req.GetSuccess(),
		ErrorCode: int(req.GetErrorCode()),
	}
}
