package modules

import (
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/components"
	acontroller "gitlab.com/GoGerman/go-jrpc/gateway/internal/modules/auth/controllers"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/modules/exchange/controllers"
	ucontrollers "gitlab.com/GoGerman/go-jrpc/gateway/internal/modules/user/controllers"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontrollers.Userer
	Exch controllers.Exchanger
}

func NewControllers(services *Services, components *components.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontrollers.NewUser(services.User, components)
	exchController := controllers.NewExchange(services.Exch, components)

	return &Controllers{
		Auth: authController,
		User: userController,
		Exch: exchController,
	}
}

func NewControllersGRPC(services *ServicesGRPC, components *components.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontrollers.NewUser(services.User, components)
	exchController := controllers.NewExchange(services.Exch, components)

	return &Controllers{
		Auth: authController,
		User: userController,
		Exch: exchController,
	}
}
