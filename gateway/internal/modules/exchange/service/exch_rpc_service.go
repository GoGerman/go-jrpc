package service

import (
	"context"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/client/rpc"
	"log"
)

type ExchangeRPCClient struct {
	client rpc.Client
}

func NewExchangeRPCClient(client rpc.Client) *ExchangeRPCClient {
	return &ExchangeRPCClient{client: client}
}

func (c *ExchangeRPCClient) History(ctx context.Context) HistoryOut {
	var result HistoryOut
	err := c.client.Call("RpcClientExchange.History", CostOut{}, &result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func (c *ExchangeRPCClient) Max(ctx context.Context) MaxOut {
	var result MaxOut
	err := c.client.Call("RpcClientExchange.Max", CostOut{}, &result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func (c *ExchangeRPCClient) Min(ctx context.Context) MinOut {
	var result MinOut
	err := c.client.Call("RpcClientExchange.Min", CostOut{}, &result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func (c *ExchangeRPCClient) Avg(ctx context.Context) AvgOut {
	var result AvgOut
	err := c.client.Call("RpcClientExchange.Avg", CostOut{}, &result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}
