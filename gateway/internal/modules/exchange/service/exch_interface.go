package service

import (
	"context"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/models"
)

type ExchangeService interface {
	History(ctx context.Context) HistoryOut
	Max(ctx context.Context) MaxOut
	Min(ctx context.Context) MinOut
	Avg(ctx context.Context) AvgOut
}

type CostOut struct {
	Coins     []models.QueryValue `json:"coins"`
	ErrorCode int                 `json:"error_code"`
}

type HistoryOut struct {
	Coins     []models.QueryValue `json:"coins"`
	ErrorCode int                 `json:"error_code"`
}

type MaxOut struct {
	Coins     []models.QueryValue `json:"coins"`
	ErrorCode int                 `json:"error_code"`
}

type MinOut struct {
	Coins     []models.QueryValue `json:"coins"`
	ErrorCode int                 `json:"error_code"`
}

type AvgOut struct {
	Coins     []models.QueryValue `json:"coins"`
	ErrorCode int                 `json:"error_code"`
}
