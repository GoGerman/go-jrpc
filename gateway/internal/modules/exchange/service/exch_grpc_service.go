package service

import (
	"context"
	"gitlab.com/GoGerman/go-jrpc/gateway/gRPC/proto"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/models"
)

type ExchangeServiceGRPCClient struct {
	client proto.ExchangeServiceRPCClient
}

func NewExchangeServiceGRPC(client proto.ExchangeServiceRPCClient) *ExchangeServiceGRPCClient {
	a := &ExchangeServiceGRPCClient{client: client}
	return a
}

func (c *ExchangeServiceGRPCClient) History(ctx context.Context) HistoryOut {
	req, err := c.client.History(ctx, &proto.HistoryRequest{})
	if err != nil {
		return HistoryOut{
			Coins:     nil,
			ErrorCode: 1,
		}
	}
	res := make([]models.QueryValue, len(req.GetCoins()))
	for i, e := range req.GetCoins() {
		res[i] = models.QueryValue{
			ID:        int(e.Id),
			Name:      e.Name,
			BuyPrice:  e.BuyPrice,
			SellPrice: e.SellPrice,
			LastTrade: e.LastTrade,
			High:      string(rune(int(e.High))),
			Low:       string(rune(int(e.Low))),
			Avg:       e.Avg,
			Vol:       e.Vol,
			VolCurr:   e.VolCurr,
			Updated:   int(e.Updated),
		}
	}
	return HistoryOut{
		Coins:     res,
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (c *ExchangeServiceGRPCClient) Max(ctx context.Context) MaxOut {
	req, err := c.client.Max(ctx, &proto.MaxRequest{})
	if err != nil {
		return MaxOut{
			Coins:     nil,
			ErrorCode: 1,
		}
	}
	res := make([]models.QueryValue, len(req.GetCoins()))
	for i, e := range req.GetCoins() {
		res[i] = models.QueryValue{
			ID:        int(e.Id),
			Name:      e.Name,
			BuyPrice:  e.BuyPrice,
			SellPrice: e.SellPrice,
			LastTrade: e.LastTrade,
			High:      string(rune(int(e.High))),
			Low:       string(rune(int(e.Low))),
			Avg:       e.Avg,
			Vol:       e.Vol,
			VolCurr:   e.VolCurr,
			Updated:   int(e.Updated),
		}
	}
	return MaxOut{
		Coins:     res,
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (c *ExchangeServiceGRPCClient) Min(ctx context.Context) MinOut {
	req, err := c.client.Min(ctx, &proto.MinRequest{})
	if err != nil {
		return MinOut{
			Coins:     nil,
			ErrorCode: 1,
		}
	}
	res := make([]models.QueryValue, len(req.GetCoins()))
	for i, e := range req.GetCoins() {
		res[i] = models.QueryValue{
			ID:        int(e.Id),
			Name:      e.Name,
			BuyPrice:  e.BuyPrice,
			SellPrice: e.SellPrice,
			LastTrade: e.LastTrade,
			High:      string(rune(int(e.High))),
			Low:       string(rune(int(e.Low))),
			Avg:       e.Avg,
			Vol:       e.Vol,
			VolCurr:   e.VolCurr,
			Updated:   int(e.Updated),
		}
	}
	return MinOut{
		Coins:     res,
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (c *ExchangeServiceGRPCClient) Avg(ctx context.Context) AvgOut {
	req, err := c.client.Avg(ctx, &proto.AvgRequest{})
	if err != nil {
		return AvgOut{
			Coins:     nil,
			ErrorCode: 1,
		}
	}
	res := make([]models.QueryValue, len(req.GetCoins()))
	for i, e := range req.GetCoins() {
		res[i] = models.QueryValue{
			ID:        int(e.Id),
			Name:      e.Name,
			BuyPrice:  e.BuyPrice,
			SellPrice: e.SellPrice,
			LastTrade: e.LastTrade,
			High:      string(rune(int(e.High))),
			Low:       string(rune(int(e.Low))),
			Avg:       e.Avg,
			Vol:       e.Vol,
			VolCurr:   e.VolCurr,
			Updated:   int(e.Updated),
		}
	}
	return AvgOut{
		Coins:     res,
		ErrorCode: int(req.GetErrorCode()),
	}
}
