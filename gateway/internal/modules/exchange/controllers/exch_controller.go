package controllers

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/components"
	errors "gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/error"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/handlers"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/responder"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/modules/exchange/service"
	"net/http"
)

type Exchanger interface {
	History(http.ResponseWriter, *http.Request)
	Max(http.ResponseWriter, *http.Request)
	Min(http.ResponseWriter, *http.Request)
	Avg(http.ResponseWriter, *http.Request)
}

type Exchange struct {
	exchange service.ExchangeService
	responder.Responder
	godecoder.Decoder
}

func NewExchange(exchange service.ExchangeService, components *components.Components) Exchanger {
	return &Exchange{
		exchange:  exchange,
		Responder: components.Responder,
		Decoder:   components.Decoder,
	}
}

func (e *Exchange) History(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		e.ErrorBadRequest(writer, err)
		return
	}
	out := e.exchange.History(request.Context())
	if out.ErrorCode != errors.NoError {
		e.OutputJSON(writer, ExchangeResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving exchange error",
			},
		})
		return
	}

	e.OutputJSON(writer, ExchangeResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}

func (e *Exchange) Max(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		e.ErrorBadRequest(writer, err)
		return
	}
	out := e.exchange.Max(request.Context())
	if out.ErrorCode != errors.NoError {
		e.OutputJSON(writer, ExchangeResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving exchange error",
			},
		})
		return
	}

	e.OutputJSON(writer, ExchangeResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}

func (e *Exchange) Min(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		e.ErrorBadRequest(writer, err)
		return
	}
	out := e.exchange.Min(request.Context())
	if out.ErrorCode != errors.NoError {
		e.OutputJSON(writer, ExchangeResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving exchange error",
			},
		})
		return
	}

	e.OutputJSON(writer, ExchangeResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}

func (e *Exchange) Avg(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		e.ErrorBadRequest(writer, err)
		return
	}
	out := e.exchange.Avg(request.Context())
	if out.ErrorCode != errors.NoError {
		e.OutputJSON(writer, ExchangeResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving exchange error",
			},
		})
		return
	}

	e.OutputJSON(writer, ExchangeResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}
