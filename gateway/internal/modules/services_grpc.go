package modules

import (
	"gitlab.com/GoGerman/go-jrpc/gateway/gRPC/proto"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/client/grpc"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/components"
	a "gitlab.com/GoGerman/go-jrpc/gateway/internal/modules/auth/service"
	e "gitlab.com/GoGerman/go-jrpc/gateway/internal/modules/exchange/service"
	u "gitlab.com/GoGerman/go-jrpc/gateway/internal/modules/user/service"
)

type ServicesGRPC struct {
	User *u.UserServiceGRPC
	Auth *a.AuthServiceGRPC
	Exch *e.ExchangeServiceGRPCClient
}

func NewServicesGRPC(components *components.Components) *ServicesGRPC {
	userGRPC := grpc.NewGRPC(components.Conf.UserRPC, components.Logger)
	userClient := proto.NewUserServiceRPCClient(userGRPC.Client)
	userService := u.NewUserServiceGRPC(userClient)

	authGRPC := grpc.NewGRPC(components.Conf.AuthRPC, components.Logger)
	authClient := proto.NewAuthServiceRPCClient(authGRPC.Client)
	authService := a.NewAuthServiceGRPC(authClient)

	exchGRPC := grpc.NewGRPC(components.Conf.ExchRPC, components.Logger)
	exchClient := proto.NewExchangeServiceRPCClient(exchGRPC.Client)
	exchService := e.NewExchangeServiceGRPC(exchClient)

	return &ServicesGRPC{
		User: userService,
		Auth: authService,
		Exch: exchService,
	}
}
