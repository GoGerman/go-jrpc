package modules

import (
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/client/rpc"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/components"
	aservcie "gitlab.com/GoGerman/go-jrpc/gateway/internal/modules/auth/service"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/modules/exchange/service"
	uservice "gitlab.com/GoGerman/go-jrpc/gateway/internal/modules/user/service"
)

type Services struct {
	User uservice.Userer
	Auth aservcie.Auther
	Exch service.ExchangeService
}

func NewServices(components *components.Components) *Services {
	userClient := rpc.NewJSONRPC(components.Conf.UserRPC, components.Logger)
	userClientRPC := uservice.NewUserServiceJSONRPC(userClient)

	authClient := rpc.NewJSONRPC(components.Conf.AuthRPC, components.Logger)
	authClientRPC := aservcie.NewAuthServiceJSONRPC(authClient)

	exchClient := rpc.NewJSONRPC(components.Conf.ExchRPC, components.Logger)
	exchClientRPC := service.NewExchangeRPCClient(exchClient)
	return &Services{
		User: userClientRPC,
		Auth: authClientRPC,
		Exch: exchClientRPC,
	}
}
