package components

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/GoGerman/go-jrpc/gateway/config"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/responder"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/tools/cryptography"
	"go.uber.org/zap"
)

type Components struct {
	Conf         config.AppConf
	Responder    responder.Responder
	Decoder      godecoder.Decoder
	Logger       *zap.Logger
	TokenManager cryptography.TokenManager
	Hash         cryptography.Hasher
}

func NewComponents(conf config.AppConf, responder responder.Responder, decoder godecoder.Decoder, logger *zap.Logger, token cryptography.TokenManager, hash cryptography.Hasher) *Components {
	return &Components{Conf: conf, Responder: responder, Decoder: decoder, Logger: logger, TokenManager: token, Hash: hash}
}
