package docs

import controller "gitlab.com/GoGerman/go-jrpc/gateway/internal/modules/exchange/controllers"

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

// swagger:route GET /api/1/exchange/history exchange exchangeHistoryRequest
// Хранение истории цен криптовалюты.
// security:
//   - Bearer: []
// responses:
//   200: exchangeHistoryResponse

// swagger:response exchangeHistoryResponse
//
//nolint:all
type exchangeHistoryResponse struct {
	// in:body
	Body []controller.ExchangeResponse
}

// swagger:route GET /api/1/exchange/max exchange exchangeMaxRequest
// Получение списка пар криптовалют с максимальной ценой.
// security:
//   - Bearer: []
// responses:
//   200: exchangeMaxResponse

// swagger:response exchangeMaxResponse
//
//nolint:all
type exchangeMaxResponse struct {
	// in:body
	Body []controller.ExchangeResponse
}

// swagger:route GET /api/1/exchange/min exchange exchangeMinRequest
// Получение списка пар криптовалют с минимальной ценой.
// security:
//   - Bearer: []
// responses:
//   200: exchangeMinResponse

// swagger:response exchangeMinResponse
//
//nolint:all
type exchangeMinResponse struct {
	// in:body
	Body []controller.ExchangeResponse
}

// swagger:route GET /api/1/exchange/avg exchange exchangeAvgRequest
// Вывод списка пар криптовалют со средней ценой.
// security:
//   - Bearer: []
// responses:
//   200: exchangeAvgResponse

// swagger:response exchangeAvgResponse
//
//nolint:all
type exchangeAvgResponse struct {
	// in:body
	Body []controller.ExchangeResponse
}
