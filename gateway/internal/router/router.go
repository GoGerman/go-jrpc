package router

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/components"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/middleware"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/modules"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *components.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
			r.Route("/auth", func(r chi.Router) {
				authController := controllers.Auth
				r.Post("/register", authController.Register)
				r.Post("/login", authController.Login)
				r.Route("/refresh", func(r chi.Router) {
					r.Use(authCheck.CheckRefresh)
					r.Post("/", authController.Refresh)
				})
				r.Post("/verify", authController.Verify)
			})
			r.Route("/user", func(r chi.Router) {
				userController := controllers.User
				r.Route("/profile", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", userController.Profile)
					r.Post("/changePassword", userController.ChangePassword)
				})
			})
			r.Route("/exchange", func(r chi.Router) {
				exchController := controllers.Exch
				r.Use(authCheck.CheckStrict)
				r.Get("/min", exchController.Min)
				r.Get("/max", exchController.Max)
				r.Get("/avg", exchController.Avg)
				r.Get("/history", exchController.History)
			})
		})
	})

	return r
}
