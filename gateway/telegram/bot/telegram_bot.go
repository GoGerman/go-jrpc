package bot

import (
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
)

func InitBot() (*tgbotapi.BotAPI, int64) {
	bot, err := tgbotapi.NewBotAPI("6071909185:AAFlF81-3Y68mDHBTiaRZ6Rw5GWbbGVpMNU")
	if err != nil {
		log.Fatal(err)
	}

	updateConfig := tgbotapi.NewUpdate(0)
	updateConfig.Timeout = 10

	updates, err := bot.GetUpdatesChan(updateConfig)
	if err != nil {
		log.Fatal(err)
	}

	for update := range updates {
		// Проверяем, является ли обновление новым сообщением
		if update.Message == nil {
			continue
		}

		// Получаем ChatID из входящего сообщения
		chatID := update.Message.Chat.ID
		return bot, chatID
	}
	return nil, 0
}

func RunBot(message string, bot *tgbotapi.BotAPI, chatID int64) {
	// Создаем новое сообщение для отправки
	msg := tgbotapi.NewMessage(chatID, message)

	// Отправляем сообщение
	_, err := bot.Send(msg)
	if err != nil {
		log.Fatal(err)
	}
}
