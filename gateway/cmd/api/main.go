package main

import (
	"github.com/joho/godotenv"
	config2 "gitlab.com/GoGerman/go-jrpc/gateway/config"
	logger2 "gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/logger"
	"gitlab.com/GoGerman/go-jrpc/gateway/run"
	"os"
)

func main() {
	// env
	err := godotenv.Load()
	// конф приложения
	config := config2.NewAppConfig()
	// логер
	logger := logger2.NewLogger(config, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}
	config.Init(logger)
	app := run.NewApp(config, logger)

	exitCode := app.Bootstrap().Run()
	os.Exit(exitCode)
}
