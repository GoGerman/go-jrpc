package config

import (
	"go.uber.org/zap"
	"os"
	"strconv"
	"time"
)

const (
	AppName = "APP_NAME"

	serverPort         = "SERVER_PORT"
	envShutdownTimeout = "SHUTDOWN_TIMEOUT"
	envAccessTTL       = "ACCESS_TTL"
	envRefreshTTL      = "REFRESH_TTL"
	envVerifyLinkTTL   = "VERIFY_LINK_TTL"

	parseShutdownTimeoutError    = "config: parse server shutdown timeout error"
	parseRpcShutdownTimeoutError = "config: parse rpc server shutdown timeout error"
	parseTokenTTlError           = "config: parse token ttl error"
)

type AppConf struct {
	AppName     string    `yaml:"app_name"`
	Environment string    `yaml:"environment"`
	Domain      string    `yaml:"domain"`
	APIUrl      string    `yaml:"api_url"`
	Server      Server    `yaml:"server"`
	Token       Token     `yaml:"token"`
	Cors        Cors      `yaml:"cors"`
	Logger      Logger    `yaml:"logger"`
	RPCServer   RPCServer `yaml:"rpc_server"`
	UserRPC     RPCClient `yaml:"client_rpc"`
	AuthRPC     RPCClient `yaml:"client_rpc"`
	ExchRPC     RPCClient `yaml:"client_rpc"`
}

type RPCClient struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
}

type RPCServer struct {
	Port         string        `yaml:"port"`
	ShutdownTime time.Duration `yaml:"shutdown_timeout"`
	Type         string        `yaml:"type"` // grpc or jsonrpc or http
}

type Logger struct {
	Level string `yaml:"level"`
}

type Token struct {
	AccessTTL     time.Duration `yaml:"access_ttl"`
	RefreshTTL    time.Duration `yaml:"refresh_ttl"`
	AccessSecret  string        `yaml:"access_secret"`
	RefreshSecret string        `yaml:"refresh_secret"`
}

type Cors struct {
	// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
	AllowedOrigins   []string `yaml:"allowed_origins"`
	AllowedMethods   []string `yaml:"allowed_methods"`
	AllowedHeaders   []string `yaml:"allowed_headers"`
	ExposedHeaders   []string `yaml:"exposed_headers"`
	AllowCredentials bool     `yaml:"allow_credentials"`
	MaxAge           int      `yaml:"max_age"` // Maximum value not ignored by any of major browsers
}

func newCors() *Cors {
	return &Cors{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"https://*", "http://*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}
}

func NewAppConfig() AppConf {
	port := os.Getenv(serverPort)
	return AppConf{
		AppName: os.Getenv(AppName),
		Server: Server{
			Port: port,
		},
		Cors: *newCors(),
	}
}

type Server struct {
	Port            string        `yaml:"port"`
	ShutdownTimeout time.Duration `yaml:"shutdown_timeout"`
}

func (a *AppConf) Init(logger *zap.Logger) {
	shutDownTimeOut, err := strconv.Atoi(os.Getenv(envShutdownTimeout))
	if err != nil {
		logger.Fatal(parseShutdownTimeoutError)
	}
	shutDownTimeout := time.Duration(shutDownTimeOut) * time.Second
	if err != nil {
		logger.Fatal(parseRpcShutdownTimeoutError)
	}

	var accessTTL int
	accessTTL, err = strconv.Atoi(os.Getenv(envAccessTTL))
	if err != nil {
		logger.Fatal(parseTokenTTlError)
	}
	a.Token.AccessTTL = time.Duration(accessTTL) * time.Minute
	var refreshTTL int
	refreshTTL, err = strconv.Atoi(os.Getenv(envRefreshTTL))
	if err != nil {
		logger.Fatal(parseTokenTTlError)
	}
	//var verifyLinkTTL int
	//verifyLinkTTL, err = strconv.Atoi(os.Getenv(envVerifyLinkTTL))
	//if err != nil {
	//	logger.Fatal(parseTokenTTlError)
	//}

	a.RPCServer.Port = os.Getenv("RPC_PORT")
	a.RPCServer.Type = os.Getenv("RPC_SERVER_TYPE")

	a.UserRPC.Host = os.Getenv("USER_RPC_HOST")
	a.UserRPC.Port = os.Getenv("USER_RPC_PORT")

	a.AuthRPC.Host = os.Getenv("AUTH_RPC_HOST")
	a.AuthRPC.Port = os.Getenv("AUTH_RPC_PORT")

	a.ExchRPC.Host = os.Getenv("EXCH_RPC_HOST")
	a.ExchRPC.Port = os.Getenv("EXCH_RPC_PORT")

	a.Token.AccessSecret = os.Getenv("ACCESS_SECRET")
	a.Token.RefreshSecret = os.Getenv("REFRESH_SECRET")

	a.Server.ShutdownTimeout = shutDownTimeout

	a.Token.RefreshTTL = time.Duration(refreshTTL) * time.Hour * 24
}
