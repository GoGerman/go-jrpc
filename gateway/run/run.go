package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/GoGerman/go-jrpc/gateway/config"
	components2 "gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/components"
	error2 "gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/error"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/responder"
	router2 "gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/router"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/server"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/infrastracture/tools/cryptography"
	"gitlab.com/GoGerman/go-jrpc/gateway/internal/modules"
	"gitlab.com/GoGerman/go-jrpc/gateway/telegram/notifier"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"net/http"
	"os"
)

type Aplication interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap() Runner
}

type App struct {
	conf        config.AppConf
	logger      *zap.Logger
	srv         server.Server
	service     *modules.Services
	serviceGRPC *modules.ServicesGRPC
	//jsonRPC server.Server
	Sig chan os.Signal
}

func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return error2.GeneralError
	}

	return error2.NoError
}

func (a *App) Bootstrap() Runner {
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := components2.NewComponents(a.conf, responseManager, decoder, a.logger, tokenManager, hash)

	var controllers *modules.Controllers

	go func() {
		notifier.RabbitMqRun()
	}()

	switch a.conf.RPCServer.Type {
	case "rpc":
		// инициализация сервисов
		services := modules.NewServices(components)
		a.service = services

		controllers = modules.NewControllers(services, components)
	case "grpc":
		// инициализация сервисов
		services := modules.NewServicesGRPC(components)
		a.serviceGRPC = services

		controllers = modules.NewControllersGRPC(services, components)
	default:
		a.logger.Fatal("wrong rpc server type, fix .env file")
	}
	// инициализация роутера
	var r *chi.Mux
	r = router2.NewRouter(controllers, components)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}
