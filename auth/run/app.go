package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/GoGerman/go-jrpc/auth/config"
	models2 "gitlab.com/GoGerman/go-jrpc/auth/export/models"
	"gitlab.com/GoGerman/go-jrpc/auth/gRPC/proto"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/db"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/cache"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/component"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/db/migrate"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/db/scanner"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/errors"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/responder"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/router"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/server"
	internal "gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/service"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/tools/cryptography"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/modules"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/modules/auth/service"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/provider"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/storages"
	"gitlab.com/GoGerman/go-jrpc/auth/rpc/auth"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"net/http"
	"net/rpc"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf         config.AppConf
	logger       *zap.Logger
	srv          server.Server
	jsonRPC      server.Server
	GRPC         server.Server
	Sig          chan os.Signal
	Storages     *storages.Storages
	Services     *modules.Services
	ServicesGRPC *modules.ServicesGRPC
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	switch a.conf.RPCServer.Type {
	case `rpc`:
		// запускаем http сервер
		errGroup.Go(func() error {
			err := a.srv.Serve(ctx)
			if err != nil && err != http.ErrServerClosed {
				a.logger.Error("app: server error", zap.Error(err))
				return err
			}
			return nil
		})
		//запускаем rpc сервер
		errGroup.Go(func() error {
			err := a.jsonRPC.Serve(ctx)
			if err != nil {
				a.logger.Error("app: server error", zap.Error(err))
				return err
			}
			return nil
		})

		if err := errGroup.Wait(); err != nil {
			return errors.GeneralError
		}
	case `grpc`:
		//запускаем Grpc сервер
		errGroup.Go(func() error {
			err := a.GRPC.Serve(ctx)
			if err != nil {
				a.logger.Error("app: GRPC server error", zap.Error(err))
				return err
			}
			return nil
		})

		if err := errGroup.Wait(); err != nil {
			return errors.GeneralError
		}
	}
	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		//&models2.UserDTO{},
		&models2.EmailVerifyDTO{},
		//&models2.TestDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	// инициализация кэша основанного на redis
	cacheClient, err := cache.NewCache(a.conf.Cache, decoder, a.logger)
	if err != nil {
		a.logger.Fatal("error init cache", zap.Error(err))
	}

	// инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter, cacheClient)
	a.Storages = newStorages

	var controllers *modules.Controllers

	switch a.conf.RPCServer.Type {
	case "rpc":
		services := modules.NewServices(newStorages, components)
		a.Services = services
		// инициализация сервиса Auth в json RPC
		authRPC := auth.NewAuthServiceJSONRPC(services.Auth)
		jsonRPCServer := rpc.NewServer()
		err = jsonRPCServer.Register(authRPC)
		if err != nil {
			a.logger.Fatal("error init auth json RPC", zap.Error(err))
		}
		// инициализация сервера json RPC
		a.jsonRPC = server.NewJSONRPC(a.conf.RPCServer, jsonRPCServer, a.logger)

		controllers = modules.NewControllers(services, components)

		// инициализация роутера
		var r *chi.Mux
		r = router.NewRouter(controllers, components)
		// конфигурация сервера
		srv := &http.Server{
			Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
			Handler: r,
		}
		// инициализация сервера
		a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	case "grpc":
		services := modules.NewServicesGRPC(components, a.Storages)
		a.Services = services

		grpcAuth := service.NewGRCPServerAuth(services.Auth)
		grpcServer := grpc.NewServer()
		proto.RegisterAuthServiceRPCServer(grpcServer, grpcAuth)

		a.GRPC = server.NewGRPCserver(a.conf.RPCServer, grpcServer, a.logger)
	default:
		a.logger.Fatal("err in type of server")
	}
	// возвращаем приложение
	return a
}
