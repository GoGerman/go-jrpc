package service

import (
	"context"
	"gitlab.com/GoGerman/go-jrpc/auth/gRPC/proto"
)

type GRCPServerAuth struct {
	proto.UnimplementedAuthServiceRPCServer
	a Auther
}

func NewGRCPServerAuth(a Auther) *GRCPServerAuth {
	return &GRCPServerAuth{a: a}
}

func (g *GRCPServerAuth) Register(ctx context.Context, in *proto.RegisterIn) (*proto.RegisterOut, error) {
	out := g.a.Register(ctx, RegisterIn{
		Email:    in.GetEmail(),
		Password: in.GetPassword(),
	})
	return &proto.RegisterOut{
		Status:    int32(out.Status),
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (g *GRCPServerAuth) AuthorizeEmail(ctx context.Context, in *proto.AuthorizeEmailIn) (*proto.AuthorizeOut, error) {
	out := g.a.AuthorizeEmail(ctx, AuthorizeEmailIn{
		Email:          in.GetEmail(),
		Password:       in.GetPassword(),
		RetypePassword: in.GetRetypePassword(),
	})
	return &proto.AuthorizeOut{
		UserId:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (g *GRCPServerAuth) AuthorizeRefresh(ctx context.Context, in *proto.AuthorizeRefreshIn) (*proto.AuthorizeOut, error) {
	out := g.a.AuthorizeRefresh(ctx, AuthorizeRefreshIn{
		UserID: int(in.GetUserId()),
	})

	return &proto.AuthorizeOut{
		UserId:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (g *GRCPServerAuth) AuthorizePhone(ctx context.Context, in *proto.AuthorizePhoneIn) (*proto.AuthorizeOut, error) {
	out := g.a.AuthorizePhone(ctx, AuthorizePhoneIn{
		Phone: in.GetPhone(),
		Code:  int(in.GetCode()),
	})
	return &proto.AuthorizeOut{
		UserId:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (g *GRCPServerAuth) SendPhoneCode(ctx context.Context, in *proto.SendPhoneCodeIn) (*proto.SendPhoneCodeOut, error) {
	out := g.a.SendPhoneCode(ctx, SendPhoneCodeIn{Phone: in.GetPhone()})
	return &proto.SendPhoneCodeOut{
		Phone: out.Phone,
		Code:  int32(out.Code),
	}, nil

}

func (g *GRCPServerAuth) VerifyEmail(ctx context.Context, in *proto.VerifyEmailIn) (*proto.VerifyEmailOut, error) {
	out := g.a.VerifyEmail(ctx, VerifyEmailIn{
		Hash:  in.GetHash(),
		Email: in.GetEmail(),
	})
	return &proto.VerifyEmailOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}
