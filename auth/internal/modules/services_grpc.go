package modules

import (
	"gitlab.com/GoGerman/go-jrpc/auth/gRPC/proto"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/client/grpc"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/component"
	a "gitlab.com/GoGerman/go-jrpc/auth/internal/modules/auth/service"
	u "gitlab.com/GoGerman/go-jrpc/auth/internal/modules/user/service"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/storages"
)

type ServicesGRPC struct {
	User          u.Userer
	Auth          a.Auther
	UserClientRPC u.Userer
}

func NewServicesGRPC(components *component.Components, storages *storages.Storages) *Services {
	userGRPC := grpc.NewGRPC(components.Conf.UserRPC, components.Logger)
	userClient := proto.NewUserServiceRPCClient(userGRPC.Client)
	userService := u.NewUserServiceGRPC(userClient)

	authService := a.NewAuth(userService, storages.Verify, components)

	return &Services{
		User: userService,
		Auth: authService,
	}
}
