package modules

import (
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/client/rpc"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/infrastructure/component"
	aservice "gitlab.com/GoGerman/go-jrpc/auth/internal/modules/auth/service"
	uservice "gitlab.com/GoGerman/go-jrpc/auth/internal/modules/user/service"
	"gitlab.com/GoGerman/go-jrpc/auth/internal/storages"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	//userService := uservice.NewUserService(storages.User, components.Logger)
	userRPC := rpc.NewJSONRPC(components.Conf.UserRPC, components.Logger)
	userClientRPC := uservice.NewUserServiceJSONRPC(userRPC)
	return &Services{
		User: userClientRPC,
		Auth: aservice.NewAuth(userClientRPC, storages.Verify, components),
	}
}
