package grpc

import (
	"fmt"
	"gitlab.com/GoGerman/go-jrpc/auth/config"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"sync"
)

type ClientGRPC struct {
	conf   config.RPCClient
	logger *zap.Logger
	Client *grpc.ClientConn
	sync.Mutex
}

func NewGRPC(conf config.RPCClient, logger *zap.Logger) *ClientGRPC {
	// попытка проинициализировать rpc клиента при старте приложения
	userClient, err := grpc.Dial(fmt.Sprintf("%s:%s", conf.Host, conf.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		logger.Fatal("error init grpc client", zap.Error(err))
	}
	logger.Info("grpc client user connected")

	return &ClientGRPC{conf: conf, Client: userClient, logger: logger}
}
