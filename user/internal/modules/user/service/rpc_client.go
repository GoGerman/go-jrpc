package service

import (
	"context"
	"github.com/lib/pq"
	"gitlab.com/GoGerman/go-jrpc/user/export/models"
	"gitlab.com/GoGerman/go-jrpc/user/internal/infrastructure/errors"
	"gitlab.com/GoGerman/go-jrpc/user/internal/infrastructure/tools/cryptography"
	"gitlab.com/GoGerman/go-jrpc/user/internal/modules/user/storage"
	"go.uber.org/zap"
)

type UserService struct {
	storage storage.Userer
	logger  *zap.Logger
}

func NewUserService(storage storage.Userer, logger *zap.Logger) *UserService {
	return &UserService{storage: storage, logger: logger}

}

func (u *UserService) Create(ctx context.Context, in UserCreateIn) UserCreateOut {
	var dto models.UserDTO
	dto.SetName(in.Name).
		SetPhone(in.Phone).
		SetEmail(in.Email).
		SetPassword(in.Password).
		SetEmailVerified(true). // временно, пока не работает верификация по email
		SetRole(in.Role)

	userID, err := u.storage.Create(ctx, dto)
	if err != nil {
		if v, ok := err.(*pq.Error); ok && v.Code == "23505" {
			return UserCreateOut{
				ErrorCode: errors.UserServiceUserAlreadyExists,
			}
		}
		return UserCreateOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return UserCreateOut{
		UserID: userID,
	}
}

func (u *UserService) Update(ctx context.Context, in UserUpdateIn) UserUpdateOut {
	panic("implement me")
}

func (u *UserService) VerifyEmail(ctx context.Context, in UserVerifyEmailIn) UserUpdateOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return UserUpdateOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}
	dto.SetEmailVerified(true)
	err = u.storage.Update(ctx, dto)
	if err != nil {
		u.logger.Error("user: update err", zap.Error(err))
		return UserUpdateOut{
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}

	return UserUpdateOut{
		Success: true,
	}
}

func (u *UserService) ChangePassword(ctx context.Context, in ChangePasswordIn) ChangePasswordOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)

	if !cryptography.CheckPassword(dto.GetPassword(), in.OldPassword) {
		return ChangePasswordOut{
			ErrorCode: errors.UserServiceChangePasswordErr,
		}
	}

	hashPass, err := cryptography.HashPassword(in.NewPassword)

	dto.SetPassword(hashPass)

	if err = u.storage.Update(ctx, dto); err != nil {
		u.logger.Error("parser: update err", zap.Error(err))
		return ChangePasswordOut{
			ErrorCode: errors.UserServiceChangePasswordErr,
		}
	}

	return ChangePasswordOut{
		Success:   true,
		ErrorCode: 0,
	}
}

func (u *UserService) GetByEmail(ctx context.Context, in GetByEmailIn) UserOut {
	userDTO, err := u.storage.GetByEmail(ctx, in.Email)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return UserOut{
		User: &models.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.GetRole(),
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByPhone(ctx context.Context, in GetByPhoneIn) UserOut {
	panic("implement me")
}

func (u *UserService) GetByID(ctx context.Context, in GetByIDIn) UserOut {
	userDTO, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return UserOut{
		User: &models.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.Role,
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByIDs(ctx context.Context, in GetByIDsIn) UsersOut {
	panic("implement me")
}
