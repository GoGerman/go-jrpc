package service

import (
	"context"
	"gitlab.com/GoGerman/go-jrpc/user/export/models"
	"gitlab.com/GoGerman/go-jrpc/user/gRPC/proto"
)

type UserGrpcServer struct {
	user Userer
	proto.UnimplementedUserServiceRPCServer
}

func NewUserGrpcServer(u Userer) *UserGrpcServer {
	return &UserGrpcServer{user: u}
}

func (u *UserGrpcServer) Create(ctx context.Context, in *proto.UserCreateIn) (*proto.UserCreateOut, error) {
	out := u.user.Create(ctx, UserCreateIn{
		Name:     in.GetName(),
		Phone:    in.GetPhone(),
		Email:    in.GetEmail(),
		Password: in.GetPassword(),
		Role:     int(in.GetRole()),
	})
	return &proto.UserCreateOut{
		UserId:    int32(out.UserID),
		ErrorCode: int32(out.ErrorCode),
	}, nil
}
func (u *UserGrpcServer) Update(ctx context.Context, in *proto.UserUpdateIn) (*proto.UserUpdateOut, error) {
	out := u.user.Update(ctx, UserUpdateIn{
		User: models.User{
			ID:            int(in.GetUser().Id),
			Name:          in.GetUser().Name,
			Phone:         in.GetUser().Phone,
			Email:         in.GetUser().Email,
			Password:      in.GetUser().Password,
			Role:          int(in.GetUser().Role),
			Verified:      in.GetUser().Verified,
			EmailVerified: in.GetUser().EmailVerified,
			PhoneVerified: in.GetUser().PhoneVerified,
		},
		Fields: nil,
	})
	return &proto.UserUpdateOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserGrpcServer) VerifyEmail(ctx context.Context, in *proto.UserVerifyEmailIn) (*proto.UserUpdateOut, error) {
	out := u.user.VerifyEmail(ctx, UserVerifyEmailIn{UserID: int(in.GetUserId())})
	return &proto.UserUpdateOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserGrpcServer) ChangePassword(ctx context.Context, in *proto.ChangePasswordIn) (*proto.ChangePasswordOut, error) {
	out := u.user.ChangePassword(ctx, ChangePasswordIn{
		UserID:      int(in.GetUserId()),
		OldPassword: in.GetOldPassword(),
		NewPassword: in.GetNewPassword(),
	})
	return &proto.ChangePasswordOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserGrpcServer) GetByEmail(ctx context.Context, in *proto.GetByEmailIn) (*proto.UserOut, error) {
	out := u.user.GetByEmail(ctx, GetByEmailIn{Email: in.GetEmail()})
	return &proto.UserOut{
		User: &proto.User{
			Id:            int32(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int32(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserGrpcServer) GetByPhone(ctx context.Context, in *proto.GetByPhoneIn) (*proto.UserOut, error) {
	out := u.user.GetByPhone(ctx, GetByPhoneIn{Phone: in.GetPhone()})
	return &proto.UserOut{
		User: &proto.User{
			Id:            int32(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int32(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}
func (u *UserGrpcServer) GetByID(ctx context.Context, in *proto.GetByIDIn) (*proto.UserOut, error) {
	out := u.user.GetByID(ctx, GetByIDIn{UserID: int(in.GetUserId())})
	return &proto.UserOut{
		User: &proto.User{
			Id:            int32(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int32(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}
func (u *UserGrpcServer) GetByIDs(ctx context.Context, in *proto.GetByIDsIn) (*proto.UsersOut, error) {
	users := make([]*proto.User, len(in.GetUserIds()))
	sl32 := make([]int, len(in.GetUserIds()))
	for i := range in.GetUserIds() {
		sl32[i] = int(in.GetUserIds()[i])
	}
	out := u.user.GetByIDs(ctx, GetByIDsIn{UserIDs: sl32})

	for _, user := range out.User {
		users = append(users, &proto.User{
			Id:            int32(user.ID),
			Name:          user.Name,
			Phone:         user.Phone,
			Email:         user.Email,
			Password:      user.Password,
			Role:          int32(user.Role),
			Verified:      user.Verified,
			EmailVerified: user.EmailVerified,
			PhoneVerified: user.PhoneVerified,
		})
	}

	return &proto.UsersOut{
		User:      users,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}
