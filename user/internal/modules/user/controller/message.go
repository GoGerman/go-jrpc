package controller

import (
	"gitlab.com/GoGerman/go-jrpc/user/export/models"
)

//go:generate easytags $GOFILE
type RegisterRequest struct {
	Email          string `json:"email"`
	Password       string `json:"password"`
	RetypePassword string `json:"retype_password"`
	IdempotencyKey string `json:"idempotency_key"`
}

type ProfileResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"error_code,omitempty"`
	Data      Data `json:"data"`
}

type Data struct {
	Message string      `json:"message,omitempty"`
	User    models.User `json:"auth,omitempty"`
}

type ChangePasswordRequest struct {
	OldPassword string `json:"old_password"`
	NewPassword string `json:"new_password"`
}

type ChangePasswordResponse struct {
	Success   bool   `json:"success"`
	ErrorCode int    `json:"error_code,omitempty"`
	Message   string `json:"message"`
}
