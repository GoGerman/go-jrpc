package modules

import (
	"gitlab.com/GoGerman/go-jrpc/user/internal/infrastructure/component"
	ucontroller "gitlab.com/GoGerman/go-jrpc/user/internal/modules/user/controller"
)

type Controllers struct {
	//Auth acontroller.Auther
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	//authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		//Auth: authController,
		User: userController,
	}
}
