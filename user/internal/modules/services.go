package modules

import (
	"gitlab.com/GoGerman/go-jrpc/user/internal/infrastructure/component"

	"gitlab.com/GoGerman/go-jrpc/user/internal/modules/user/service"
	"gitlab.com/GoGerman/go-jrpc/user/internal/storages"
)

type Services struct {
	User service.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{

		User: service.NewUserService(storages.User, components.Logger),
	}
}
