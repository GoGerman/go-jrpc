package storages

import (
	"gitlab.com/GoGerman/go-jrpc/user/internal/db/adapter"
	"gitlab.com/GoGerman/go-jrpc/user/internal/infrastructure/cache"
	//vstorage "gitlab.com/GoGerman/go-jrpc/user/internal/modules/auth/storage"
	ustorage "gitlab.com/GoGerman/go-jrpc/user/internal/modules/user/storage"
)

type Storages struct {
	User ustorage.Userer
	//Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User: ustorage.NewUserStorage(sqlAdapter, cache),
		//Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
